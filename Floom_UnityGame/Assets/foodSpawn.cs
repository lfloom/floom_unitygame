﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI; 

public class foodSpawn : MonoBehaviour
{
    public GameObject Object1;
    public GameObject Object2;
    public GameObject Object3;
    public GameObject Object4;
    public GameObject Object5;
    public GameObject Object6;
    public GameObject Object7;
    public GameObject Object8;
    public GameObject Object9;
    public GameObject Object10;
    public int spawnnum;
    public Text scoreText;
    private int count;
    private int counter; 

    //to change image
    public Image ImageComponent;
    public Image ImageComponent2;
    public Image ImageComponent3; 
    public Sprite FirstImage;
    public Sprite SecondImage;
    public Sprite ThirdImage;
    public Sprite ForthImage;
    public Sprite FifthImage;
    public Sprite SixthImage;
    public Sprite SeventhImage;
    public Sprite EigthImage;
    public Sprite NinthImage;
    public Sprite TenthImage;
    public Sprite ImageEmpty;

    //game over text
    public Text GameOverText;


    // Start is called before the first frame update
    void Start()
    {
        

        //used for images
        counter = 0;

        //make score go up
        count = 0;
        Score();

        //spawn random objects 
        int x = 0;
        while (x < spawnnum)
        {
            SpawnRandomObject();
            x++;
        }

        //set first images
        changeImageOne();
        changeImageTwo();
        changeImageThree();

        //gameovertext
        //game over text
        GameOverText.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {

    }

    void SpawnRandomObject()
    {
        int i = Random.Range(1, 11);
        GameObject obj;
        if (i == 1)
        {
            obj = Object1;
            //obj.name = "1AvocadoSrite"; 
        }
        else if (i == 2)
        {
            obj = Object2;
            //obj.name = "1BurgerSprite";
        }

        else if (i == 3)
        {
            obj = Object3;
            //obj.name = "1BurgerSprite";
        }

        else if (i == 4)
        {
            obj = Object4;
            //obj.name = "1ChickenSprite";
        }

        else if (i == 5)
        {
            obj = Object5;
            //obj.name = "1EggSprite";
        }

        else if (i == 6)
        {
            obj = Object6;
           // obj.name = "1FrySprite";
        }

        else if (i == 7)
        {
            obj = Object7;
            //obj.name = "1PineappleSprite";
        }

        else if (i == 8)
        {
            obj = Object8;
            //obj.name = "1TempuraSprite";
        }

        else if (i == 9)
        {
            obj = Object9;
           // obj.name = "1TomatoSprite";
        }

        else
        {
            obj = Object10;
           // obj.name = "1WatermelonSprite";
        }

        Instantiate(obj, new Vector3(Random.Range(-8f, 8f), Random.Range(-4f, 2.5f), -9.25f), Quaternion.identity);
    }

    void OnCollisionEnter2D(Collision2D other)
    {

        //spawn random object 
        if ((other.gameObject.CompareTag("Avocado") && (ImageComponent.sprite.name == "Food_Avocado")) ||
            (other.gameObject.CompareTag("Avocado") && (ImageComponent2.sprite.name == "Food_Avocado")) ||
            (other.gameObject.CompareTag("Avocado") && (ImageComponent3.sprite.name == "Food_Avocado")))
        {
            other.gameObject.SetActive(false);
            counter++;
            SpawnRandomObject();
            SpawnRandomObject();
            SpawnRandomObject();

            if (ImageComponent.sprite.name == "Food_Avocado")
            {
                ImageComponent.sprite = ImageEmpty;
            }
            else if (ImageComponent2.sprite.name == "Food_Avocado")
            {
                ImageComponent2.sprite = ImageEmpty;
            }
            else
            {
                ImageComponent3.sprite = ImageEmpty;
            }

            gameObject.transform.localScale = new Vector3(1, 1, 1);

        }

        if ((other.gameObject.CompareTag("Banana") && (ImageComponent.sprite.name == "Food_Banana")) ||
            (other.gameObject.CompareTag("Banana") && (ImageComponent2.sprite.name == "Food_Banana")) ||
            (other.gameObject.CompareTag("Banana") && (ImageComponent3.sprite.name == "Food_Banana")))

        {
            other.gameObject.SetActive(false);
            //ImageComponent.sprite = null;
            counter++;
            SpawnRandomObject();

            if (ImageComponent.sprite.name == "Food_Banana")
            {
                ImageComponent.sprite = ImageEmpty;
            }
            else if (ImageComponent2.sprite.name == "Food_Banana")
            {
                ImageComponent2.sprite = ImageEmpty;
            }
            else
            {
                ImageComponent3.sprite = ImageEmpty;
            }

        }

        if ((other.gameObject.CompareTag("Burger") && (ImageComponent.sprite.name == "Food_Burger")) ||
            (other.gameObject.CompareTag("Burger") && (ImageComponent2.sprite.name == "Food_Burger")) ||
            (other.gameObject.CompareTag("Burger") && (ImageComponent3.sprite.name == "Food_Burger")))
        {
            other.gameObject.SetActive(false);
            //ImageComponent.sprite = null;
            counter++;
            SpawnRandomObject();

            if (ImageComponent.sprite.name == "Food_Burger")
            {
                ImageComponent.sprite = ImageEmpty;
            }
            else if (ImageComponent2.sprite.name == "Food_Burger")
            {
                ImageComponent2.sprite = ImageEmpty;
            }
            else
            {
                ImageComponent3.sprite = ImageEmpty;
            }
        }

        if ((other.gameObject.CompareTag("Chicken") && (ImageComponent.sprite.name == "Food_Chicken")) ||
            (other.gameObject.CompareTag("Chicken") && (ImageComponent2.sprite.name == "Food_Chicken")) ||
            (other.gameObject.CompareTag("Chicken") && (ImageComponent3.sprite.name == "Food_Chicken")))
        {
            other.gameObject.SetActive(false);
            //ImageComponent.sprite = null;
            counter++;
            SpawnRandomObject();

            if (ImageComponent.sprite.name == "Food_Chicken")
            {
                ImageComponent.sprite = ImageEmpty;
            }
            else if (ImageComponent2.sprite.name == "Food_Chicken")
            {
                ImageComponent2.sprite = ImageEmpty;
            }
            else
            {
                ImageComponent3.sprite = ImageEmpty;
            }

        }

        if ((other.gameObject.CompareTag("Egg") && (ImageComponent.sprite.name == "Food_Egg")) ||
            (other.gameObject.CompareTag("Egg") && (ImageComponent2.sprite.name == "Food_Egg")) ||
            (other.gameObject.CompareTag("Egg") && (ImageComponent3.sprite.name == "Food_Egg")))
        {
            other.gameObject.SetActive(false);
            //ImageComponent.sprite = null;
            counter++;
            SpawnRandomObject();

            if (ImageComponent.sprite.name == "Food_Egg")
            {
                ImageComponent.sprite = ImageEmpty;
            }
            else if (ImageComponent2.sprite.name == "Food_Egg")
            {
                ImageComponent2.sprite = ImageEmpty;
            }
            else
            {
                ImageComponent3.sprite = ImageEmpty;
            }

        }

        if ((other.gameObject.CompareTag("Fry") && (ImageComponent.sprite.name == "Food_Fries")) ||
            (other.gameObject.CompareTag("Fry") && (ImageComponent2.sprite.name == "Food_Fries")) ||
            (other.gameObject.CompareTag("Fry") && (ImageComponent3.sprite.name == "Food_Fries")))
        {
            other.gameObject.SetActive(false);
            //ImageComponent.sprite = null;
            counter++;
            SpawnRandomObject();

            if (ImageComponent.sprite.name == "Food_Fries")
            {
                ImageComponent.sprite = ImageEmpty;
            }
            else if (ImageComponent2.sprite.name == "Food_Fries")
            {
                ImageComponent2.sprite = ImageEmpty;
            }
            else
            {
                ImageComponent3.sprite = ImageEmpty;
            }

        }

        if ((other.gameObject.CompareTag("Pineapple") && (ImageComponent.sprite.name == "Food_Pineapple")) ||
            (other.gameObject.CompareTag("Pineapple") && (ImageComponent2.sprite.name == "Food_Pineapple")) ||
            (other.gameObject.CompareTag("Pineapple") && (ImageComponent3.sprite.name == "Food_Pineapple")))
        {
            other.gameObject.SetActive(false);
            //ImageComponent.sprite = null;
            counter++;
            SpawnRandomObject();

            if (ImageComponent.sprite.name == "Food_Pineapple")
            {
                ImageComponent.sprite = ImageEmpty;
            }
            else if (ImageComponent2.sprite.name == "Food_Pineapple")
            {
                ImageComponent2.sprite = ImageEmpty;
            }
            else
            {
                ImageComponent3.sprite = ImageEmpty;
            }

        }

        if ((other.gameObject.CompareTag("Tempura") && (ImageComponent.sprite.name == "Food_Tempura")) ||
            (other.gameObject.CompareTag("Tempura") && (ImageComponent2.sprite.name == "Food_Tempura")) ||
            (other.gameObject.CompareTag("Tempura") && (ImageComponent3.sprite.name == "Food_Tempura")))
        {
            other.gameObject.SetActive(false);
            //ImageComponent.sprite = null;
            counter++;
            SpawnRandomObject();

            if (ImageComponent.sprite.name == "Food_Tempura")
            {
                ImageComponent.sprite = ImageEmpty;
            }
            else if (ImageComponent2.sprite.name == "Food_Tempura")
            {
                ImageComponent2.sprite = ImageEmpty;
            }
            else
            {
                ImageComponent3.sprite = ImageEmpty;
            }

        }

        if ((other.gameObject.CompareTag("Tomato") && (ImageComponent.sprite.name == "Food_Tomato")) ||
            (other.gameObject.CompareTag("Tomato") && (ImageComponent2.sprite.name == "Food_Tomato")) ||
            (other.gameObject.CompareTag("Tomato") && (ImageComponent3.sprite.name == "Food_Tomato")))
        {
            other.gameObject.SetActive(false);
            //ImageComponent.sprite = null;
            counter++;
            SpawnRandomObject();

            if (ImageComponent.sprite.name == "Food_Tomato")
            {
                ImageComponent.sprite = ImageEmpty;
            }
            else if (ImageComponent2.sprite.name == "Food_Tomato")
            {
                ImageComponent2.sprite = ImageEmpty;
            }
            else
            {
                ImageComponent3.sprite = ImageEmpty;
            }

        }

        else if ((other.gameObject.CompareTag("Watermelon") && (ImageComponent.sprite.name == "Food_Watermelon")) ||
            (other.gameObject.CompareTag("Watermelon") && (ImageComponent2.sprite.name == "Food_Watermelon")) ||
            (other.gameObject.CompareTag("Watermelon") && (ImageComponent3.sprite.name == "Food_Watermelon")))
        {
            other.gameObject.SetActive(false);
            //ImageComponent.sprite = null;
            counter++;
            SpawnRandomObject();

            if (ImageComponent.sprite.name == "Food_Watermelon")
            {
                ImageComponent.sprite = ImageEmpty;
            }
            else if (ImageComponent2.sprite.name == "Food_Watermelon")
            {
                ImageComponent2.sprite = ImageEmpty;
            }
            else
            {
                ImageComponent3.sprite = ImageEmpty;
            }

        }

        else if(other.gameObject.CompareTag("Drink"))
            {
                other.gameObject.SetActive(false);
                gameObject.transform.localScale = new Vector3(4, 4, 4);
                count = count + 100;
                Score();

            int x = 0; 
            while (x < 15)
            {
                SpawnRandomObject(); 
                x++;
            }
 
            }
        else if (other.gameObject.CompareTag("Coconut"))
        {
            gameObject.SetActive(false);
            GameOverText.enabled = true; 
            

        }

        else
        {
            other.gameObject.SetActive(false);
            SpawnRandomObject();
        }

        //score goes up
        if (counter == 3)
        {
            count = count + 5;
            Score();
            changeImageOne();
            changeImageTwo();
            changeImageThree();
            counter = 0;
        }

    }

    void Score()
    {
        scoreText.text = "Score: " + count.ToString();
        /*if (count >= 12)
        {
            winText.text = "You Win!";
        }
        */
    }

    void changeImageOne() //method to change images 
    {
        int i = Random.Range(1, 11);
        if (i == 1)
        {
            ImageComponent.sprite = FirstImage;
        }
        else if (i == 2)
        {
            ImageComponent.sprite = SecondImage;
        }

        else if (i == 3)
        {
            ImageComponent.sprite = ThirdImage;
        }

        else if (i == 4)
        {
            ImageComponent.sprite = ForthImage;
        }

        else if (i == 5)
        {
            ImageComponent.sprite = FifthImage;
        }

        else if (i == 6)
        {
            ImageComponent.sprite = SixthImage;
        }

        else if (i == 7)
        {
            ImageComponent.sprite = SeventhImage;
        }

        else if (i == 8)
        {
            ImageComponent.sprite = EigthImage;
        }

        else if (i == 9)
        {
            ImageComponent.sprite = NinthImage;
        }

        else
        {
            ImageComponent.sprite = TenthImage;
        }

    }


    //change second image
    void changeImageTwo() //method to change images 
    {
        int i = Random.Range(1, 11);
        if (i == 1)
        {
            ImageComponent2.sprite = FirstImage;
        }
        else if (i == 2)
        {
            ImageComponent2.sprite = SecondImage;
        }

        else if (i == 3)
        {
            ImageComponent2.sprite = ThirdImage;
        }

        else if (i == 4)
        {
            ImageComponent2.sprite = ForthImage;
        }

        else if (i == 5)
        {
            ImageComponent2.sprite = FifthImage;
        }

        else if (i == 6)
        {
            ImageComponent2.sprite = SixthImage;
        }

        else if (i == 7)
        {
            ImageComponent2.sprite = SeventhImage;
        }

        else if (i == 8)
        {
            ImageComponent2.sprite = EigthImage;
        }

        else if (i == 9)
        {
            ImageComponent2.sprite = NinthImage;
        }

        else
        {
            ImageComponent2.sprite = TenthImage;
        }

    }

    //change third image
    void changeImageThree() //method to change images 
    {
        int i = Random.Range(1, 11);
        if (i == 1)
        {
            ImageComponent3.sprite = FirstImage;
        }
        else if (i == 2)
        {
            ImageComponent3.sprite = SecondImage;
        }

        else if (i == 3)
        {
            ImageComponent3.sprite = ThirdImage;
        }

        else if (i == 4)
        {
            ImageComponent3.sprite = ForthImage;
        }

        else if (i == 5)
        {
            ImageComponent3.sprite = FifthImage;
        }

        else if (i == 6)
        {
            ImageComponent3.sprite = SixthImage;
        }

        else if (i == 7)
        {
            ImageComponent3.sprite = SeventhImage;
        }

        else if (i == 8)
        {
            ImageComponent3.sprite = EigthImage;
        }

        else if (i == 9)
        {
            ImageComponent3.sprite = NinthImage;
        }

        else
        {
            ImageComponent3.sprite = TenthImage;
        }

    }

}

