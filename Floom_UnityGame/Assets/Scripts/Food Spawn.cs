﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FoodSpawn : MonoBehaviour
{
    public GameObject Object1;
    public GameObject Object2;
    public GameObject Object3;
    public GameObject Object4;
    public GameObject Object5;
    public GameObject Object6;
    public GameObject Object7;
    public GameObject Object8;
    public GameObject Object9;
    public GameObject Object10;
    public int spawnnum;

    // Start is called before the first frame update
    void Start()
    {
        int x = 0;
        while(x < spawnnum)
        {
            SpawnRandomObject(); 
        }
    }

    // Update is called once per frame
    void Update()
    {
       
    }

    void SpawnRandomObject()
    {
        int i = Random.Range(1, 10);
        GameObject obj;
        if (i == 1)
        {
            obj = Object1;
        }
        else if(i == 2)
        {
            obj = Object2;
        }

        else if (i == 3)
        {
            obj = Object3;
        }

        else if (i == 4)
        {
            obj = Object4;
        }

        else if (i == 5)
        {
            obj = Object5;
        }

        else if (i == 6)
        {
            obj = Object6;
        }

        else if (i == 7)
        {
            obj = Object7;
        }

        else if (i == 8)
        {
            obj = Object8;
        }

        else if (i == 9)
        {
            obj = Object9;
        }

        else
        {
            obj = Object10;
        }

        Instantiate(obj, new Vector3(transform.position.x, 0, 0), Quaternion.identity);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Food"))
        {
            other.gameObject.SetActive(false);
        }

        SpawnRandomObject();
    }
}
