﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_Movement : MonoBehaviour
{
    //variables public and hidden
    [HideInInspector] public bool facingRight = true;
   // [HideInInspector] public bool facingDown = true;
    public GameObject Character;
    public float Speed = 5f;
 

    // Update is called once per frame
    void Update()
    {
        //this is for left and right
        float h = Input.GetAxis("Horizontal") * Speed; //gets position going left and right and gives player a speed to go 

        Character.transform.Translate(h * Time.deltaTime, 0, 0); //setting player location based on where they are on the screen

        //this is for up and down 
        float k = Input.GetAxis("Vertical") * Speed;

        Character.transform.Translate(0, k * Time.deltaTime, 0);
    }

    void FixedUpdate()
    {
        float h = Input.GetAxis("Horizontal");
        float k = Input.GetAxis("Vertical");

        //turns player left or right
        if (h < 0 && !facingRight) //flips player to go the opposite way 
        {
            LeftRight();
        }
        else if (h > 0 && facingRight)
        {
            LeftRight();
        }

        /*
        //turns player up and down 
        if (k < 0 && !facingRight) //flips player to go the opposite way 
        {
            UpDown();
        }
        else if (k > 0 && facingRight)
        {
            UpDown();
        }
        */

    }

    void LeftRight() //flips sprite to go in the direction that they are going with the keys. Now they face left when going left and right when going right
    {
        facingRight = !facingRight;
        Vector3 theScale = transform.localScale;
        theScale.x *= -1; //inverts it
        transform.localScale = theScale;

    }

    /*
    void UpDown() //flips sprite to go in the direction that they are going with the keys. Now they face up when going up and down when going down 
    {
        facingDown = !facingDown;
        Quaternion theRotation = transform.localRotation;
        theRotation.z = 180;
        transform.localRotation = theRotation;
    }
    */
}
