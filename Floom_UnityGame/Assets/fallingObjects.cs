﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class fallingObjects : MonoBehaviour
{

    public GameObject Coconut;
    public GameObject Drink;

    //falling objects timer
    public float maxTime = 5;
    public float minTime = 2;

    //time currently stored
    private float time;

    //object spawned at this time
    private float spawnTime;

    //when a coconut falls on the player they die
    //when a drink falls on them they get really big for a second and get points for everyfood they pick up
    // Start is called before the first frame update
    void Start()
    {
        SetRandomTime();
        time = minTime;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void FixedUpdate()
    {

        //Counts up
        time += Time.deltaTime;

        //Check if its the right time to spawn the object
        if (time >= spawnTime)
        {
            SpawnObject();
            SetRandomTime();
        }

    }

    //Spawns the object and resets the time
    void SpawnObject()
    {
        time = minTime;
        int i = Random.Range(1, 3);
        GameObject obj;
        if (i == 1)
        {
            obj = Coconut;
        }
        else
        {
            obj = Drink;
        }

        Instantiate(obj, new Vector3(Random.Range(-6f, 8f), 2.5f, -9.25f), Quaternion.identity);
    }

    //Sets the random time between minTime and maxTime
    void SetRandomTime()
        {
            spawnTime = Random.Range(minTime, maxTime);
        }

    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            other.gameObject.SetActive(false); 

            //Then Display Loser Text 
        }
    }
}
