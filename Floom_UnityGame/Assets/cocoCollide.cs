﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class cocoCollide : MonoBehaviour
{
    public Text GameOverText;

    

    // Start is called before the first frame update
    void Start()
    {
        GameOverText.enabled = false;

       
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnCollisionEnter2D(Collision2D other)
    {
        if ((other.gameObject.CompareTag("Avocado")) || (other.gameObject.CompareTag("Banana")) || (other.gameObject.CompareTag("Chicken")) ||
            (other.gameObject.CompareTag("Egg")) || (other.gameObject.CompareTag("Fry")) || (other.gameObject.CompareTag("Burger")) ||
            (other.gameObject.CompareTag("Tomato")) || (other.gameObject.CompareTag("Tempura")) || (other.gameObject.CompareTag("Watermelon")) ||
            (other.gameObject.CompareTag("Pineapple")))
        {
            other.gameObject.SetActive(false);
        }
    }
}
